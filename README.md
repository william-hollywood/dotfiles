# dotfiles

Ansible dot files for deployment of my config

# Usage

The current possible make arguments:

- `dotfiles`

Ran by executing `make` followed by the argument.

# Playbooks

Each argument corresponds to an ansible playbook. Eg: `make dotfiles` -> `dotfiles.yml`

The approximate structure of the playbook is as follows:

```yml
---
- name: 'name'
  hosts: localhost
  connection: local
  roles:
#    - role: 'role1'
#    - role: 'role2'
#    - role: 'role3'
```

Each of the roles are (should be) commented out by default so that before executing `make 'argument'` you can select what modules you want to install.

# Roles

## zsh-install

This role:
- Ensures `zsh` package is installed
- Ensures [OMZ](https://github.com/ohmyzsh/ohmyzsh) is installed
  - Removes excess files/plugins I dont need/want incase extra plugins are added
- Installs the required plugins if they're not already installed

## zsh-conf
- Copies my zsh theme (`raw/custom-minimal.zsh-theme`) from `roles/zsh/templates/custom-minimal.zsh-theme.j2`
- Copies my `.zshrc` (`raw/.zshrc`) from `roles/zsh/templates/.zshrc.j2`
  - `.zshrc` sets the user colour for the theme, colours are set for users in the following task (inside of `roles/zsh/tasks/main.yml`)
    ```yml
    - name: Set custom colours
    set_fact: 
        colours:
        will: "#1793D1"
        root: "#FF0000"
        default: "#FFFFFF"
    ```
  - Theme colouring of the name is set using hex codes Eg: `export COL=#FFFFFF` or to reset to your default colour a `$DEFAULT_COL` environment variable is set.

# Requirements

In order to run this you need [ansible](https://www.ansible.com/) installed.

## `requirements.yml`

Inside here is where [ansible galaxy](https://galaxy.ansible.com/) roles can be put if they are used in for any of my other roles. There are currently none of them being used, just thought it'd be a nice this incase I need it in the future.
